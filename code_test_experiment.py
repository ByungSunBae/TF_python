import numpy as np
import os
import sys

import tensorflow as tf
from tensorflow import layers
from tensorflow.contrib.learn import Experiment
from Datasets.datasets import train_input_fn
from Datasets.datasets import mnist
from Datasets.datasets import valid_input_fn
from Datasets.datasets import test_input_fn
from estimator.models import lenet5_model

tf.logging.set_verbosity(tf.logging.INFO)

model_dir = "./lenet5"
params = {
	'rate': 0.5,
	'n_classes': 10
}

log_step_n = 10
summary_step = 10
keep_max_into = 10
check_step = 10

config = tf.estimator.RunConfig()

new_config = config.replace(log_step_count_steps = log_step_n,
		keep_checkpoint_max = keep_max_into,
		save_summary_steps = summary_step,
		save_checkpoints_steps = check_step)

classifier_est = tf.estimator.Estimator(model_fn=lenet5_model, model_dir=model_dir, params=params, config=new_config)

#tensors_to_log = {
#	"loss": "cross_entropy",
#	"accuracy": "acc_op"
#}

#logging_hook = tf.train.LoggingTensorHook(
#		tensors=tensors_to_log, every_n_iter=10)

# classifier.train(input_fn=train_input_fn, steps=500, hooks=[logging_hook])
#train_spec = tf.estimator.TrainSpec(input_fn=train_input_fn, max_steps=500, hooks=[train_logging_hook])
#eval_spec = tf.estimator.EvalSpec(input_fn=valid_input_fn, steps=None, hooks=[valid_logging_hook])
#tf.estimator.train_and_evaluate(classifier_est, train_spec, eval_spec)

exp = Experiment(
		estimator=classifier_est,
		train_input_fn=train_input_fn,
		eval_input_fn=valid_input_fn,
		train_steps=500,
		eval_steps=None,
		#eval_hooks= [logging_hook]
		)

exp.train_and_evaluate()
test_scores = classifier_est.evaluate(input_fn=test_input_fn)
print('Accuracy (lenet5_model): {0:f}'.format(test_scores['my_accuracy']))

if __name__ == '__unit_test__':
	tf.app.run()
