import tensorflow as tf
from tensorflow import layers
import numpy as np
import os
import sys

X_FEATURE = 'x'

def conv2d_and_maxpool2d(inputs, filters, kernel_size, padding, activation, pool_size, pool_strides, pool_padding, scope):
	with tf.variable_scope(scope):
		h_conv = layers.conv2d(
				inputs,
				filters=filters,
				kernel_size=kernel_size,
				padding=padding,
				activation=activation)
		h_pool = layers.max_pooling2d(
				h_conv, pool_size=pool_size, strides=pool_strides, padding=pool_padding)
	return h_pool

def lenet5_model(features, labels, mode, params):
	feature = tf.reshape(features[X_FEATURE], [-1, 28, 28, 1])

	rate = params['rate']
	n_classes = params['n_classes']
	
	net1 = conv2d_and_maxpool2d(feature, filters=32, kernel_size=[5,5], padding='same', activation=tf.nn.relu, 
			pool_size=2, pool_strides=2, pool_padding='same', scope="conv1")

	net2 = conv2d_and_maxpool2d(net1, filters=32, kernel_size=[5,5], padding='same', activation=tf.nn.relu, 
			pool_size=2, pool_strides=2, pool_padding='same', scope="conv2")

	net3 = conv2d_and_maxpool2d(net2, filters=32, kernel_size=[5,5], padding='same', activation=tf.nn.relu, 
			pool_size=2, pool_strides=2, pool_padding='same', scope="conv3")
	
	#total_shape = 1
	#for d_shape in net3.shape.as_list():
	#	if d_shape is None:
	#		next
	#	else:
	#		total_shape *= d_shape

	net3_flat = layers.flatten(net3) 

	h_fc1 = layers.dense(net3_flat, 512, activation=tf.nn.relu)
	h_fc1 = layers.dropout(
			h_fc1, 
			rate=rate, 
			training=(mode == tf.estimator.ModeKeys.TRAIN))

	logits = layers.dense(h_fc1, n_classes, activation=None)

	predicted_classes = tf.argmax(logits, 1)
	predictions = {
			'class': predicted_classes,
			'prob': tf.nn.softmax(logits, name="softmax_tensor")
		}
	if mode == tf.estimator.ModeKeys.PREDICT:
		return tf.estimator.EstimatorSpec(mode, predictions=predictions)

	loss = tf.losses.sparse_softmax_cross_entropy(labels=labels, logits=logits)
	accuracy = tf.metrics.accuracy(labels=labels, predictions=predicted_classes)
	
	eval_metrics = {
		'my_accuracy': accuracy,
		#'eval_loss': tf.losses.sparse_softmax_cross_entropy(labels=labels, logits=logits)
	}
	#train_metrics= {
	#	'train_accuracy': accuracy,
		#'train_loss': loss
	#}
	tf.identity(loss, name='cross_entropy')
	tf.identity(accuracy[1], name='acc_op')
	# tf.identity(accuracy[1], name='eval_acc')

	tf.summary.scalar('my_accuracy', accuracy[1])
	#tf.summary.scalar('loss', loss)

	if mode == tf.estimator.ModeKeys.EVAL:
		return tf.estimator.EstimatorSpec(
				mode, loss=loss, eval_metric_ops=eval_metrics)

	if mode == tf.estimator.ModeKeys.TRAIN:
		optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.01)
		train_op = optimizer.minimize(loss, global_step=tf.train.get_global_step())
		return tf.estimator.EstimatorSpec(mode, loss=loss, train_op=train_op)
