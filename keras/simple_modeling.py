# keras example in tensorflow
# mnist cnn example
import tensorflow as tf
from tensorflow import keras
import numpy as np
import os
import sys

Sequential = keras.models.Sequential
ModelCheckpoint = keras.callbacks.ModelCheckpoint
EarlyStopping = keras.callbacks.EarlyStopping
TensorBoard = keras.callbacks.TensorBoard

keras_layers = {
	'Dense': keras.layers.Dense,
	'Dropout': keras.layers.Dropout,
	'Flatten': keras.layers.Flatten,
	'Conv2D': keras.layers.Conv2D,
	'MaxPooling2D': keras.layers.MaxPooling2D
}

K = keras.backend

mnist = keras.datasets.mnist
(x_train, y_train), (x_test, y_test) = mnist.load_data("../mnist")

batch_size = 128
num_classes = 10
epochs = 5

img_rows, img_cols = 28, 28

if K.image_data_format() == 'channels_first':
	x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
	x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
	input_shape = (1, img_rows, img_cols)
else:
	x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
	x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
	input_shape = (img_rows, img_cols, 1)

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255.
x_test /= 255.

print('x_train shape:', x_train.shape)
print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')

y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

model = Sequential()
model.add(keras_layers['Conv2D'](32, kernel_size=(3,3),
			activation='relu',
			input_shape=input_shape))
model.add(keras_layers['Conv2D'](64, (3,3), activation='relu'))
model.add(keras_layers['MaxPooling2D'](pool_size=(2,2)))
model.add(keras_layers['Dropout'](0.25))
model.add(keras_layers['Flatten']())
model.add(keras_layers['Dense'](128, activation='relu'))
model.add(keras_layers['Dropout'](0.5))
model.add(keras_layers['Dense'](num_classes, activation='softmax'))

model.compile(loss=keras.losses.categorical_crossentropy,
		optimizer=keras.optimizers.Adadelta(),
		metrics=['accuracy'])
save_path = "./mnist_model"
if not os.path.exists(save_path):
	os.makedirs(save_path, exist_ok=True)

file_path = save_path + "/" + "cnn.best.h5"
checkpoint = ModelCheckpoint(file_path, monitor='val_acc', verbose=1, save_best_only=True, save_weights_only=False, mode='max')
earlystopping = EarlyStopping(monitor='val_loss', min_delta=1e-7, patience=0, verbose=0, mode='auto')
tensorboard = TensorBoard(log_dir="./logs", histogram_freq=0, batch_size=batch_size, write_graph=True, write_grads=False, write_images=False)

callbacks_list = [checkpoint, earlystopping, tensorboard]

try:
	model.fit(x_train, y_train,
			validation_split=0.33,
			batch_size=batch_size,
			epochs=epochs,
			verbose=1, 
			callbacks=callbacks_list)

	score = model.evaluate(x_test, y_test, verbose=0)
	print('Test loss:', score[0])
	print('Test accuracy:', score[1])
except KeyboardInterrupt as e:
	save_path = "./mnist_model"
	if not os.path.exists(save_path):
		os.makedirs(save_path, exist_ok=True)

	file_path = save_path + "/" + "cnn.h5"
	keras.models.save_model(
			model,
			file_path,
			overwrite=True,
			include_optimizer=True
			)
	print("Save recent model. path : {}".format(file_path))

is_restore = True
if is_restore:
	model_restore = keras.models.load_model(file_path)
	score_restore = model_restore.evaluate(x_test, y_test, verbose=0)
	print('Test loss:', score_restore[0])
	print('Test accuracy:', score_restore[1])
else:
	model_restore = None
