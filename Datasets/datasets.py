import tensorflow as tf
from tensorflow.contrib import learn
import numpy as np

X_FEATURE='x'
BATCH_SIZE=128

mnist = learn.datasets.DATASETS['mnist']('/tmp/mnist')

train_input_fn = tf.estimator.inputs.numpy_input_fn(
		x={X_FEATURE: mnist.train.images},
		y=mnist.train.labels.astype(np.int32),
		batch_size=BATCH_SIZE,
		num_epochs=None,
		shuffle=True)

valid_input_fn = tf.estimator.inputs.numpy_input_fn(
		x={X_FEATURE: mnist.validation.images},
		y=mnist.validation.labels.astype(np.int32),
		num_epochs=1,
		shuffle=False)

test_input_fn = tf.estimator.inputs.numpy_input_fn(
		x={X_FEATURE: mnist.test.images},
		y=mnist.test.labels.astype(np.int32),
		num_epochs=1,
		shuffle=False)

eval_input_fn = tf.estimator.inputs.numpy_input_fn(
		x={X_FEATURE: mnist.train.images},
		y=mnist.train.labels.astype(np.int32),
		num_epochs=1,
		shuffle=False)

